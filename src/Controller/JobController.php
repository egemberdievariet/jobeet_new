<?php

namespace App\Controller;


use App\Entity\Category;
use App\Form\JobEditType;
use App\Form\JobType;
use App\Service\JobHistoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Job;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\FileUploader;


class JobController extends AbstractController
{

    /** @var FileUploader */
    private $uploader;

    /**
     * @param FileUploader $uploader
     */
    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * Lists all job entities.
     *
     * @Route("/", name="job.list", methods="GET")
     *
     * @param EntityManagerInterface $em
     * @param JobHistoryService $jobHistoryService
     *
     * @return Response
     */
    public function list(EntityManagerInterface $em, JobHistoryService $jobHistoryService) : Response
    {
        $categories = $em->getRepository(Category::class)->findWithActiveJobs();

        return $this->render('job/list.html.twig', [
            'categories' => $categories,
            'historyJobs' => $jobHistoryService->getJobs(),
        ]);
    }


    /**
     * @param int $id
     *
     * @return Job|null
     */
    public function findActiveJob(int $id) : ?Job
    {
        return $this->createQuery('j')
            ->where('j.id = :id')
            ->andWhere('j.expiresAt > :date')
            ->setParameter('id', $id)
            ->setParameter('date', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * Finds and displays a job entity.
     *
     * @Route("job/{id}", name="job.show", methods="GET", requirements={"id" = "\d+"})
     *
     * @Entity("job", expr="repository.findActiveJob(id)")
     *
     * @param Job $job
     * @param JobHistoryService $jobHistoryService
     *
     * @return Response
     */
    public function show(Job $job, JobHistoryService $jobHistoryService) : Response
    {
        $jobHistoryService->addJob($job);

        return $this->render('job/show.html.twig', [
            'job' => $job,
        ]);
    }

    /**
     * Creates a new job entity.
     *
     * @Route("/job/create", name="job.create", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em, FileUploader $fileUploader) : Response
    {
        $job = new Job();
        $form = $this->createForm(JobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $logoFile */
            $logoFile = $form->get('logo')->getData();

            if ($logoFile instanceof UploadedFile) {
                $fileName = $fileUploader->upload($logoFile);

                $job->setLogo($fileName);
            }

            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute(
                'job.show',
                ['id' => $job->getId()]
            );
        }

        return $this->render('job/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param int|null $categoryId
     *
     * @return Job[]
     */
    private function findCategoriesWithActiveJobs(EntityManagerInterface $em)
    {
        $qb = $em->createQueryBuilder();
        $qb->select('c')->from('App:Category', 'c')
            ->innerJoin('c.jobs', 'j')
            ->where('j.expiresAt > :dt')
            ->orderBy('c.name')
            ->setParameter('dt', new \DateTime('-30 days'));
        $jobs = $qb->getQuery()->getResult();

//        dump($jobs);
//        die();
        return $jobs;
    }

    /**
     * @param int|null $categoryId
     *
     * @return Job[]
     */
    private function findActiveJobs(EntityManagerInterface $em, int $categoryId = null)
    {
        $query = $em->createQuery(
            'SELECT j FROM App:Job j WHERE j.expiresAt > :date1a order by j.expiresAt DESC '
        )->setParameter('date1a', new \DateTime('-30 days'));
        $jobs = $query->getResult();

        return $jobs;
    }

    /**
     * Edit existing job entity
     *
     * @Route("/job/{token}/edit", name="job.edit", methods={"GET", "POST"}, requirements={"token" = "\w+"})
     *
     * @param Request $request
     * @param Job $job
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function edit(Request $request, Job $job, EntityManagerInterface $em) : Response
    {
        $fileName = "/".$job->getLogo();
        $logo = $fileName;
        $job->setLogo($logo);
        $form = $this->createForm(JobEditType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return $this->redirectToRoute(
                'job.show',
                ['id' => $job->getId()]
            );
        }

        return $this->render('job/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new job entity.
     *
     * @Route("/job/search", name="job.search", methods={ "GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function search(Request $request, EntityManagerInterface $em) : Response
    {

        $searchq = $request->request->get("search");
        $output = '';
        if($searchq!="") {

            $searchq = preg_replace("#[^0-9a-z]#i", "", $searchq);

            dump($searchq);
            $query = $em->createQueryBuilder()
                ->select('j')
                ->from('App:Job', 'j')
                ->where('j.company like :s')
                ->orWhere('j.location like :s')
                ->setParameter('s', '%'.$searchq.'%')
                ->getQuery();

            $jobs = $query->getResult();

        }
        else {
            return $this->redirectToRoute("job.list");
        }

        return $this->render('job/search_result.html.twig', [
            'jobs' => $jobs,
        ]);
    }


    /**
     * Creates a form to delete a job entity.
     *
     * @param Job $job
     *
     * @return FormInterface
     */
    private function createDeleteForm(Job $job) : FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('job.delete', ['token' => $job->getToken()]))
            ->setMethod('DELETE')
            ->getForm();
    }
    /**
     * Delete a job entity.
     *
     * @Route("job/{token}/delete", name="job.delete", methods="DELETE", requirements={"token" = "\w+"})
     *
     * @param Request $request
     * @param Job $job
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function delete(Request $request, Job $job, EntityManagerInterface $em) : Response
    {
        $form = $this->createDeleteForm($job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($job);
            $em->flush();
        }

        return $this->redirectToRoute('job.list');
    }
    /**
     * Publish a job entity.
     *
     * @Route("job/{id}/publish", name="job.publish", methods="POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param Job $job
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function publish(Request $request, Job $job, EntityManagerInterface $em) : Response
    {
        $form = $this->createPublishForm($job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job->setActivated(true);

            $em->flush();

            $this->addFlash('notice', 'Your job was published');
        }

        return $this->redirectToRoute('job.show', [
            'id' => $job->getId(),
        ]);
    }


    /**
     * Creates a form to publish a job entity.
     *
     * @param Job $job
     *
     * @return FormInterface
     */
    private function createPublishForm(Job $job) : FormInterface
    {
        return $this->createFormBuilder(['id' => $job->getId()])
            ->setAction($this->generateUrl('job.publish', ['id' => $job->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}